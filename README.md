# Dpl (dee-pee-ell) is a deploy tool made for continuous deployment

- Based on official ruby image.
- For deployment within CI

E.g. for use with the static blog generator [Hugo](https://gohugo.io/)

```
cd git/blog
docker run -ti -v $(pwd):/src halberom/dpl --provider=pages --github-token=$GITHUB_TOKEN --local_dir=public
```

